# Linux Workshop
Bohužel image nemá `man` command, ale ještě máte --help, případně lze man pages najít online.

## Commandy k dispozici pro experimentování s textem:
- `kubectl get pods -n trino-dev`
- `kubectl describe pods -n trino-dev`
- `kubectl describe pod trino-worker-7d976b7bf4-4rn58  -n trino-dev`          // jméno pod se může změnit, pokud se tam něco restartuje, takže kdyžtak zkopírovat název z prvního příkazu
- `kubectl logs trino-worker-7d976b7bf4-4rn58 -n trino-dev`

Pokud si s tím chcete víc hrát, tak doporučuju nastavit si nějaké aliasy - klidně na celé commandy. To je takový Task 0. :)

Řešení uvádíme v base64 formátu, jedná se o příkaz, který pak můžete aplikovat. K zobrazení:
`echo "$reseni" | base64 -d`.
Případně můžete rovnou pustit pomocí:
`eval $(echo "$reseni" | base64 -d)`

Pracujte s `man` commandem pro bližší info k hintům (v Podu bohužel není, ale lze doinstalovat, což už umíte, případně zkoušet u sebe).

Task 8 a 9 se nesoustředí na žádný konkrétní vstup, použijte libovolná vlastní data, jde jen o to otestovat si pár vlastností těchto příkazů.

## Task 1
Takže jako první bod si možná zkuste zautomatizovat třetí a čtvrtý příkaz. Tj. substituujte si název podu.
Dá se získat pomocí prvního commandu, práce s textem a pipingu. Následně si vzpomeňte, jak funguje $().

Hint: `tr`, `cut`, `grep`, `alias`

- Příklad řešení pro logy: 
    1. a3ViZWN0bCBnZXQgcG9kcyAtbiB0cmluby1kZXYgfCBncmVwICJ3b3JrZXIiIHwgdHIgIiAiIFxcdCB8IGN1dCAtZjEK
    2. YWxpYXMgd29ya2VyPSdrdWJlY3RsIGxvZ3MgJChrdWJlY3RsIGdldCBwb2RzIC1uIHRyaW5vLWRldiB8IGdyZXAgIndvcmtlciIgfCB0ciAiICIgXFx0IHwgY3V0IC1mMSkgLW4gdHJpbm8tZGV2Jwo=
    3. `worker`

## Task 2
Využijte worker logy.

Vyprintěte si počet unikátních řádků, které v pátem sloupci začínají slovem "iceberg". Využijte například `echo` a zároveň libovolně popište, co děláte, tzn. výsledek ala formát "Počet: 20".

Hint: `^fraze`, `grep`, `cut`, `-s`, `uniq`, `wc`, `echo`, `$()`

- Příklad řešení:
    1. ZWNobyAiQW1vdW50IG9mICdpY2ViZXJnJyBvY2N1cmVuY2VzOiAkKHdvcmtlciB8IHRyIC1zICIgIiBcXHQgfCBjdXQgLWY1IHwgZ3JlcCAiXmljZWJlcmciIHwgdW5pcSB8IHdjIC1sKSIK

Uložte do souboru.

## Task 3
Zkuste si pohrát s hardlinky / symlinky, ať si zažijete, jak fungují. Nic extra konkrétního.
Tzn. několik hardlinků a sledovat počet. Zkusit jak fungují hardlinky a symlinky, když se smažou soubory.

Hint: `ln`, `-s`

## Task 4
Prohledat `/var` folder jako `student`. Najděte všechny soubory (ne adresáře), které mají v názvu souboru "log", permission 755 a aplikujte na ně command `ls -l`. Vytáhněte sloupce s informacemi o lincích, user+group owner, datu a pathu. Pokud dostanete chyby (stderr), zkuste je přesměrovat do "pryč". Stdout uložte do souboru "logs".
(Zkuste si to samé klidně i jako root, abyste viděli rozdíl).

Hint: `find`, `-exec`, `{} \;`, `-type`, `2>`, `-perm`, `tr`, `cut`, `{}`, `;`, `1>`

- Příklad řešení: 
    1. eyBmaW5kIC92YXIgLW5hbWUgIipsb2cqIiAtdHlwZSBmIC1wZXJtIDc1NSAtZXhlYyBscyAtbCB7fSBcOyB8IHRyIC1zICIgIiBcXHQgfCBjdXQgLWYyLTQsNi05OyB9IDI+L2Rldi9udWxsIDE+bG9nCg==

## Task 5
Vypište `/etc/password/` informace ve formátu username:userid:groupid. Seřaďte abecedně z-a. Vynechte usery, kteří mají `nologin`.

- Příklad řešení:
    1. Y2F0IC9ldGMvcGFzc3dkIHwgZ3JlcCAtdiAiL25vbG9naW4iIHwgY3V0IC1kJzonIC1mMSwzLTQgfCBzb3J0IC1yCg==

Uložte do souboru.

## Task 6
Zkuste si archivovat pomocí `tar` soubory, které jste vytvořili.

## Task 7 - výzva pro adminy
Zkuste si zreplikovat tvorbu sdíleného adresáře pro tým.

Hint: SGID, `chmod 2770` nebo `chmod g+s`, `useradd`, `groupadd`, `passwd`

## Task 8
Zkuste si základní práci s příkazem `sed`. Je na vás, jestli chcete editovat existující soubor, nebo jenom stdout.

### Subtask 1
Nahraďte čárky v nějakém náhodném .csv soubory jiným libovolným oddělovačem, např. mezerou.

- Příklad řešení:
    1. Y2F0IHBzLmNzdiB8IHNlZCAncy8sLyAvZyc=
    2. c2VkIC1pICdzLywvIC9nJyBwcy5jc3Y=
    
### Subtask 2
Odstraňte řádky, které začínají určitým textem.

- Příklad řešení:
    1. Y2F0IHBzLmNzdiB8IHNlZCAnL15mcmF6ZS9kJw==
    2. c2VkIC1pICcvXmZyYXplL2QnIHBzLmNzdg==

### Subtask 3
Nahraďte v celém dokumentu všechny okurence určitého celého slova (tj. přesný match) jiným slovem.

- Příklad řešení
    1. c2VkIC1pICdzL1w8c3RhcmVzbG92b1w+L25ldy9nJyBzb3Vib3IuY3N2

## Task 9
Zkuste si základní práci s příkazem `awk`.

### Subtask 1
Extrahujte z .csv souboru požadovaný sloupec

- Příklad řešení
    1. YXdrIC1GJywnICd7IHByaW50ICRjaXNsb3Nsb3VwY2UgfScgZGF0YS5jc3Yg
    
### Subtask 2
Sečtěte hodnoty v nějakém sloupci. Dejte si pozor na oddělovač. Příklad pro .csv soubor.

- Příklad řešení
    1. YXdrIC1GIiwnICd7IHRvdGFsICs9ICRjaXNsb3Nsb3VwY2UgfSBFTkQgeyBwcmludCAiU3VtOiAiLCB0b3RhbCB9JyBkYXRhLmNzdg==

## Task 10 
Pomocí příkazu na vypisování procesů, pipingu a práce s textem uložte do .csv souboru ve správném formátu seznam všech procesů. Zajímají nás sloupce USER, PID, PPID, TTY, TIME a COMMAND (bez argumentů). První řádek chceme nahradit nějakými lepšími nadpisy sloupců. Zajímají nás řádky, které mají přiřazeny nějaký TTY. Ignorujte řádky, které obsahují vámi použité příkazy (všechny, ne nutně jen ty, které jste spustili vy - tzn. použili jste např. "awk", ale může tam běžet i jiná instance "awk", tak ji klidně ignorujte také). Uložte do souboru.

Hint: `man ps`, `ps axo`, `tr -d`, `awk -F`, `sed`, `ps o '%p,%y'`

Upozornění: Použití `grep -v "?"` by ignorovalo řádky ve všech sloupcích. Zkuste `awk`.

- Příklad řešení:
    1. cHMgYXhvICclVSwlcCwlUCwleSwleCwlYycgfCB0ciAtZCAiICIgfCBzZWQgJzEgcy9eLiokL3V6aXZhdGVsLHByb2Nlc19pZCxyb2RpY19pZCx0ZXJtaW5hbCxjcHVfY2FzLHByaWthei8nIHwgYXdrIC1GJywnICckNCAhPSAiPyIge3ByaW50fScgfCBhd2sgLUYnLCcgJyEoJDYgfiAvXihwc3x0cnxzZWR8YXdrKS8pIHtwcmludH0nID4gcHMuY3N2

## Task 11
Procvičte si `awk`: vytáhněte z posledního sloupce vašeho souboru všechny commandy. Ignorujte nadpis. Bez duplicit.

Hint: `awk`, `NR`, `seen[$6]++`

Hint: Jde to jedním `awk` příkazem.

- Příklad řešení:
    1. YXdrIC1GJywnICdOUj4xICYmICFzZWVuWyQ2XSsrIHsgcHJpbnQgJDYgfScgcHMuY3N2